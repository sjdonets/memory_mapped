#pragma once

#include <Windows.h>
#include <string>
#include <vector>
#include <chrono>
#include <thread>

#include "Utils.h"

using FileHandle = HANDLE;
using MappingHandle = HANDLE;
using offset_type = std::ptrdiff_t;
using size_type = std::uintptr_t;
using address_type = PVOID;


constexpr FileHandle InvalidFileHandle = INVALID_HANDLE_VALUE;
constexpr MappingHandle InvalidMappingHandle = 0;

using Tstring = std::basic_string<TCHAR>;

template <typename... Args>
static Tstring to_Tstring(Args&&... args)
{
#ifndef UNICODE
    return std::to_string(std::forward<Args>(args)...);
#else
    return std::to_wstring(std::forward<Args>(args)...);
#endif
}

template<typename Enum>
constexpr auto ToIntegral(Enum val) -> typename std::underlying_type<Enum>::type
{
    return static_cast<typename std::underlying_type<Enum>::type>(val);
}

enum class FileAccess : std::uint32_t
{
    Read = GENERIC_READ,
    Write = GENERIC_WRITE,
    ReadWrite = Read | Write,

    Invalid = std::uint32_t(-1),
};

enum class FileShare : std::uint32_t
{
    None = 0,
    Read = FILE_SHARE_READ,
    Write = FILE_SHARE_WRITE,
    ReadWrite = Write | Read,
    Delete = FILE_SHARE_DELETE,

    Invalid = std::uint32_t(-1),
};

enum class FileCreation : std::uint32_t
{
    CreateNew = CREATE_NEW,
    Create = CREATE_ALWAYS,
    Open = OPEN_EXISTING,
    OpenOrCreate = OPEN_ALWAYS,
    Truncate = TRUNCATE_EXISTING,
    Append = OpenOrCreate,

    Invalid = std::uint32_t(-1),
};

enum class FileOptions : std::uint32_t
{
    None = 0,
    RandomAccess = FILE_FLAG_RANDOM_ACCESS,
    DeleteOnClose = FILE_FLAG_DELETE_ON_CLOSE,
    SequentialScan = FILE_FLAG_SEQUENTIAL_SCAN,
    Temporary = FILE_ATTRIBUTE_TEMPORARY,

    Invalid = std::uint32_t(-1),
};

enum class MappedFileAccess : std::uint32_t
{
    ReadWrite = FILE_MAP_READ | FILE_MAP_WRITE,
    Read = FILE_MAP_READ,
    Write = FILE_MAP_WRITE,
    CopyOnWrite = FILE_MAP_COPY,

    Invalid = std::uint32_t(-1),
};

enum class HandleInheritability : std::uint32_t
{
    None,
    Inheritable,
};

template<typename E>
inline E operator|(const E &lhs, const E &rhs) { return static_cast<E>( ToIntegral(lhs) | ToIntegral(rhs)); }


struct FileInfo
{
    FileHandle     Handle{InvalidFileHandle};
    MappingHandle  MHandle{ InvalidMappingHandle };
    Tstring    FileName{};
    FileAccess Access{FileAccess::Invalid};

    void CloseMapping()
    {
        if (MHandle != InvalidMappingHandle)
        {
            ::CloseHandle(MHandle);
            MHandle = InvalidMappingHandle;
        }
    }

    void CloseFile()
    {
        if (Handle != InvalidFileHandle)
        {
            ::CloseHandle(Handle);
            Handle = InvalidFileHandle;
        }
    }
};

struct MappingInfo
{
    address_type     Base{ 0 };
    size_type        NumBytes{ 0 };
    size_type        Alignment{ 0 };
    size_type        Size{ 0 };
    MappedFileAccess Mode{ MappedFileAccess::Invalid };

    MappingInfo(address_type address, size_type num_bytes, size_type offset, size_type size, MappedFileAccess access)
        : Base{ address }
        , NumBytes{ num_bytes }
        , Alignment{ offset }
        , Size{ size }
        , Mode{ access }
    {}

    address_type GetAddress() const
    {
        return address_type(static_cast<std::uint8_t*>(Base) + Alignment);
    }

    size_type GetSize() const
    {
        return Size;
    }

    void ReleaseView()
    {
        if (Base != NULL)
        {
            ::UnmapViewOfFile(Base);
            Base = {};
            Size = {};
            Alignment = {};
            Mode = {};
            NumBytes = {};
        }
    }
};

struct FileMappingInfo
{
    FileInfo                 FInfo{};
    std::vector<MappingInfo> MInfos{};
};


static FileInfo CreateFileImpl(Tstring path, FileCreation mode, FileAccess access, FileShare share, FileOptions options)
{
    FileInfo ret{};

    std::uint32_t old_err_mode{ 0 };
    ::SetThreadErrorMode(SEM_FAILCRITICALERRORS, (LPDWORD)(&old_err_mode));
    
    if (path.empty())
        return ret;

    ret.FileName = path;
    ret.Access = access;

    LPSECURITY_ATTRIBUTES sec_attrs = NULL;

    ret.Handle = ::CreateFile(path.c_str(),
                              ToIntegral(access),
                              ToIntegral(share),
                              sec_attrs,
                              ToIntegral(mode),
                              ToIntegral(options),
                              NULL);

    if (ret.Handle == InvalidFileHandle)
    {
        std::uint32_t error_code = ::GetLastError();
        ::SetErrorMode(old_err_mode);
        throw std::runtime_error(GetErrorMessage(error_code).c_str());
    }

    if (::GetFileType(ret.Handle) != FILE_TYPE_DISK)
    {
        ret.CloseFile();
        throw std::runtime_error("Not Supported: Not a File!");
    }

    return ret;
}

static void SetFileSize(FileInfo &file_info, size_type new_size)
{
    if (file_info.Handle != InvalidFileHandle)
    {
        FILE_END_OF_FILE_INFO fi;
        fi.EndOfFile.QuadPart = new_size;
        if (!::SetFileInformationByHandle(file_info.Handle, FileEndOfFileInfo, &fi, sizeof(fi)))
        {
            // Resort to page file
            file_info.CloseFile();
        }
    }
}

static int GetPageAccess(MappedFileAccess access)
{
    switch (access)
    {
    case MappedFileAccess::ReadWrite:
        return PAGE_READWRITE;
    case MappedFileAccess::Read:
        return PAGE_READONLY;
    case MappedFileAccess::CopyOnWrite:
        return PAGE_WRITECOPY;
    default:
        throw std::invalid_argument("Not supported page access.");
    }
}

static MappingHandle CreateOrOpenImpl(
    FileHandle fileHandle,
    std::string mapName,
    HandleInheritability inheritability,
    MappedFileAccess access,
    std::uint64_t capacity)
{
    MappingHandle mappedFileHandle = InvalidMappingHandle;

    SECURITY_ATTRIBUTES *sec_attrs = nullptr;
    LARGE_INTEGER maximum_size;
    maximum_size.QuadPart = capacity;

    int num_retry = 14;
    int millisecondsTimeout = 0;
    while (num_retry > 0)
    {
        mappedFileHandle = ::CreateFileMapping(fileHandle, sec_attrs, GetPageAccess(access), maximum_size.HighPart, maximum_size.LowPart, mapName.c_str());

        const std::uint32_t last_error = ::GetLastError();
        if (mappedFileHandle == InvalidMappingHandle)
        {
            if (last_error != ERROR_ACCESS_DENIED)
                throw std::runtime_error(GetErrorMessage(last_error).c_str());

            mappedFileHandle = ::OpenFileMapping(ToIntegral(access), (ToIntegral(inheritability) & ToIntegral(HandleInheritability::Inheritable)) > 0U, mapName.c_str());
            std::uint32_t last_error1 = ::GetLastError();
            if (mappedFileHandle == InvalidMappingHandle)
            {
                if (last_error1 != ERROR_FILE_NOT_FOUND)
                    throw std::runtime_error(GetErrorMessage(last_error1).c_str());

                --num_retry;
                if (millisecondsTimeout == 0)
                {
                    millisecondsTimeout = 10;
                }
                else
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(millisecondsTimeout));
                    millisecondsTimeout *= 2;
                }
            }
            else
                break;
        }
        else
            break;
    }

    if (mappedFileHandle != InvalidMappingHandle)
        return mappedFileHandle;

    throw std::runtime_error("Invalid Operation: Cant create file mapping");
}

static std::uint32_t GetSystemPageAllocationGranularity()
{
    SYSTEM_INFO system_info;
    ::GetSystemInfo(&system_info);
    return system_info.dwAllocationGranularity;
}

static MappingInfo CreateView(FileInfo file_info, MappedFileAccess access, size_type offset, size_type size)
{
    const std::uint64_t pointer_offset = static_cast<std::uint64_t>(offset) % static_cast<std::uint64_t>(::GetSystemPageAllocationGranularity());
    const std::uint64_t correct_offset = static_cast<std::uint64_t>(offset) - pointer_offset;
    const std::uint64_t num_bytes = size == 0ULL ? 0ULL : size + pointer_offset;

    MEMORYSTATUSEX memory_status{};
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    ::GlobalMemoryStatusEx(&memory_status);

    if (num_bytes >= memory_status.ullTotalVirtual)
        throw std::runtime_error("Not Enough Memory");

    LARGE_INTEGER file_offset;
    file_offset.QuadPart = correct_offset;

    address_type mapped_address = ::MapViewOfFile(file_info.MHandle,
                                                  ToIntegral(access),
                                                  file_offset.HighPart,
                                                  file_offset.LowPart,
                                                  num_bytes);

    if (mapped_address == NULL)
        throw std::runtime_error(GetErrorMessage(::GetLastError()).c_str());

    MEMORY_BASIC_INFORMATION mem_info{};

    ::VirtualQuery(mapped_address, &mem_info, sizeof(MEMORY_BASIC_INFORMATION));
    std::uint64_t regionSize = static_cast<std::uint64_t>(mem_info.RegionSize);

    if ((mem_info.State & MEM_RESERVE) != 0 || regionSize < num_bytes)
    {
        std::uint64_t size = num_bytes == 0UL ? regionSize : num_bytes;
        ::VirtualAlloc(mapped_address, size, 4096, ToIntegral(access));
        ::GetLastError();

        MEMORY_BASIC_INFORMATION mem_info2{};
        ::VirtualQuery(mapped_address, &mem_info2, sizeof(MEMORY_BASIC_INFORMATION));
        regionSize = static_cast<std::uint64_t>(mem_info2.RegionSize);
    }

    if (size == 0LL)
        size = static_cast<std::int64_t>(regionSize) - static_cast<std::int64_t>(pointer_offset);

    return MappingInfo(mapped_address, size + pointer_offset, static_cast<std::int64_t>(pointer_offset), size, access);
}

void Flush(MappingInfo map_info, std::uint64_t capacity)
{
    static const int MaxFlushWaits = 15;
    static const int MaxFlushRetriesPerWait = 20;

    if (map_info.Base == NULL)
        return;

    BOOL success = ::FlushViewOfFile(map_info.Base, capacity);
    if (success != FALSE)
        return;

    std::uint32_t last_error = ::GetLastError();
    bool error_flag = !success && last_error == ERROR_LOCK_VIOLATION;

    for (int index1 = 0; error_flag && index1 < MaxFlushWaits; ++index1)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1 << index1));

        for (int index2 = 0; error_flag && index2 < MaxFlushRetriesPerWait; ++index2)
        {
            if (::FlushViewOfFile(map_info.Base, capacity) != FALSE)
                return;

            last_error = ::GetLastError();
            error_flag = last_error == ERROR_LOCK_VIOLATION;
        }
    }

    throw std::runtime_error(GetErrorMessage(::GetLastError()).c_str());
}


