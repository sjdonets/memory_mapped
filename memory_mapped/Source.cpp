#include <cstdlib>

#include "MMapBasic.h"

int main(int argc, char* argv[])
{
    FileMappingInfo fmi;

    fmi.FInfo = CreateFileImpl("test.dat",
                               FileCreation::OpenOrCreate,
                               FileAccess::ReadWrite,
                               FileShare::ReadWrite | FileShare::Delete,
                               FileOptions::Temporary);

    SetFileSize(fmi.FInfo, 4096);

    fmi.FInfo.MHandle = CreateOrOpenImpl(fmi.FInfo.Handle, "map_1", HandleInheritability::Inheritable, MappedFileAccess::ReadWrite, 4096);

    MappingInfo map1 = CreateView(fmi.FInfo, MappedFileAccess::ReadWrite, 13, 473);
    fmi.MInfos.push_back(map1);

    memset(map1.GetAddress(), 0xAA, map1.GetSize());

    MappingInfo map2 = CreateView(fmi.FInfo, MappedFileAccess::ReadWrite, 300, 473);
    fmi.MInfos.push_back(map2);
    memset(map2.GetAddress(), 0xBB, map2.GetSize());

    Flush(map1, map1.GetSize());
    Flush(map2, map2.GetSize());

    return EXIT_FAILURE;
}
